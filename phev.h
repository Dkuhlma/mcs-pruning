#ifndef PHEV_H_
#define PHEV_H_

#include <algorithm>
#include <fstream>
#include <iomanip>
#include <iterator>
#include <math.h>
#include <sstream>
#include <stdio.h>
#include <string>
#include <vector>

//#include <gsl/gsl_rng.h>
//#include <gsl/gsl_randist.h>

#ifndef _OPENMP
    #include "omp.h"
#endif

#include "anyoption.h"
#include "Bus.h"
#include "defs.h"
#include "Generator.h"
#include "Line.h"
#include "MTRand.h"
#include "Primes.h"
#include "RandomNumbers.h"

namespace Phev{
  
   extern void calculatePHEVLoad(
            double penetrationLevel, double rho,
            int totalVehicles, int numBuses,
            std::vector<double>& phevLoad, std::vector<double>& phevGen, MTRand& mt, pruning::PHEV_PLACEMENT PHEV_PLACEMENT = pruning::PP_EVEN_ALL_BUSES);
  
  
    extern std::string getBooleanString             (bool value);
    extern std::string getPruningMethodString	 	(pruning::PRUNING_METHOD 		pm);
    extern std::string getClassificationMethodString(pruning::CLASSIFICATION_METHOD cm);
    extern std::string getSamplingMethodString		(pruning::SAMPLING_METHOD 		ps);
    extern std::string getPHEVPlacementString		(pruning::PHEV_PLACEMENT  		pp);
    extern std::string getStoppingMethodString		(pruning::STOPPING_METHOD 		sm);
    extern std::string getPruningObjString		    (pruning::PRUNING_OBJECTIVE 	po);
    
    extern pruning::PRUNING_METHOD        getPruningMethod		  (std::string s);
    extern pruning::CLASSIFICATION_METHOD getClassificationMethod (std::string s);
    extern pruning::SAMPLING_METHOD       getSamplingMethod		  (std::string s);
    extern pruning::PHEV_PLACEMENT        getPHEVPlacement		  (std::string s);
    extern pruning::STOPPING_METHOD       getStoppingMethod		  (std::string s);
    extern pruning::PRUNING_OBJECTIVE     getPruningObj           (std::string s);
};

#endif